;; initialize package sources
(require 'package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

;; initialize use-package on non-linux platforms
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

;; note: if you want to move everything out of the ~/.emacs.d folder
;; reliably, set `user-emacs-directory` before loading no-littering!
;(setq user-emacs-directory "~/.cache/emacs")

(use-package no-littering)

;; no-littering doesn't set this by default so we must place
;; auto save files in the same path as it uses for sessions
(setq auto-save-file-name-transforms
      `((".*" ,(no-littering-expand-var-file-name "auto-save/") t)))

(setq inhibit-startup-message t)

(scroll-bar-mode -1)        ; disable visible scrollbar
(tool-bar-mode -1)          ; disable the toolbar
(tooltip-mode -1)           ; disable tooltips
(set-fringe-mode 1)         ; give some breathing room

(menu-bar-mode -1)            ; disable the menu bar

(setq ring-bell-function
        (lambda ()
          (play-sound-file "/home/etbcf/Music/beep-alert.wav"))) ; errors

 (global-set-key (kbd "C-c s t") 'org-timer-set-timer) ; org timer setter
 (setq org-clock-sound "~/Music/beep-pomodoro.wav") ; finished alert

(global-display-line-numbers-mode t)

(line-number-mode 0)

;; disable line numbers for some modes
(dolist (mode '(org-mode-hook
                term-mode-hook
                eshell-mode-hook
                vterM-mode-hook
                treemacs-mode-hook
                text-mode-hook
                shell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

  (setq history-length 25)
  (savehist-mode 1)

  (save-place-mode 1)

  ;; move customization variables to a separate file and load it
  (setq custom-file (locate-user-emacs-file "custom-vars.el"))
  (load custom-file 'noerror 'message)

  (global-auto-revert-mode 1)
  (setq global-auto-revert-non-file-buffers t)

(defvar jammymacs/default-font-size 110)

(set-face-attribute 'default nil :font "fira code retina" :height jammymacs/default-font-size)

;; set fixed pitch face
(set-face-attribute 'fixed-pitch nil :font "fira code retina" :height 130)

;; set the variable pitch face
(set-face-attribute 'variable-pitch nil :font "cantarell" :height 150 :weight 'regular)

(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

(use-package general
  :after evil
  :config
  (general-create-definer etbcf/leader-keys
    :keymaps '(normal insert visual emacs)
    :prefix "SPC"
    :global-prefix "C-SPC")
  (etbcf/leader-keys
    "t"  '(:ignore t :which-key "toggles")
    "tt" '(counsel-load-theme :which-key "choose theme")
    "cf" '(lambda () (interactive) (find-file (expand-file-name "~/.emacs.d/Emacs.org")))))

(use-package evil
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u-scroll t)
  (setq evil-want-C-i-jump nil)
  :config
  (evil-mode 1)
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
  (define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join)

  ;; use visual line motions even outside of visual-line-mode buffers
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)

  (evil-set-initial-state 'messages-buffer-mode 'normal)
  (evil-set-initial-state 'dashboard-mode 'normal))

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

(use-package doom-themes
  :init (load-theme 'doom-gruvbox t))

(global-set-key (kbd "C-x C-b") 'ibuffer)

(setq ibuffer-saved-filter-groups
      (quote (("default"
	       ("text" (mode . text-mode))
	       ("dired" (mode . dired-mode))
	       ("org" (name . "^.*org$"))

	       ("web" (or (mode . web-mode) (mode . js2-mode)))
	       ("shell" (or (mode . eshell-mode) (mode . shell-mode)))
	       ("mu4e" (name . "\*mu4e\*"))
	       ("programming" (or
			       (mode . python-mode)
			       (mode . c++-mode)
			       (mode . sh-mode)))
	       ("emacs" (or
			 (name . "^\\*scratch\\*$")
			 (name . "^\\*messages\\*$")))
	       ))))

(add-hook 'ibuffer-mode-hook
	  (lambda ()
	    (ibuffer-auto-mode 1)
	    (ibuffer-switch-to-saved-filter-groups "default")))

(use-package all-the-icons
  :if (display-graphic-p)
  :commands all-the-icons-install-fonts
  :init
  (unless (find-font (font-spec :name "all-the-icons"))
    (all-the-icons-install-fonts t)))

(use-package doom-modeline
  :init (doom-modeline-mode 1)
  :custom ((doom-modeline-height 40)))

;; workaround for size of the buffer displayed when invoking org-set-tags-command
(advice-add #'fit-window-to-buffer :before (lambda (&rest _) (redisplay t)))

;; if non-nil, a word count will be added to the selection-info modeline segment.
(setq doom-modeline-enable-word-count t)

;; major modes in which to display word count continuously.
;; also applies to any derived modes. respects `doom-modeline-enable-word-count'.
;; if it brings the sluggish issue, disable `doom-modeline-enable-word-count' or
;; remove the modes from `doom-modeline-continuous-word-count-modes'.
(setq doom-modeline-continuous-word-count-modes '(text-mode))

; whether display the total line number。
(setq doom-modeline-total-line-number nil)

;; specification of \"percentage offset\" of window through buffer.
;; see `mode-line-percent-position'.
(setq doom-modeline-percent-position nil)

(use-package which-key
  :defer 0
  :init 
  :diminish which-key-mode
  :config
  (which-key-mode)
  (setq which-key-idle-delay 2))

(use-package ivy
  :init
  (ivy-mode 1) ; Enable Ivy everywhere
  :bind 
  (("C-c <up>" . ivy-push-view)
   ("C-c <down>" . ivy-switch-view)))

(use-package ivy-rich
  :after ivy
  :init
  (ivy-rich-mode 1))

(use-package ivy-prescient
  :after counsel
  :config
  (setq prescient-sort-length-enable nil)
  (ivy-prescient-mode 1)
  (prescient-persist-mode 1))

(use-package counsel
  :bind
  (("M-y" . counsel-yank-pop))
  :after (ivy amx)
  :config
  (setq counsel-yank-pop-preselect-last t)
  (setq counsel-yank-pop-separator "\n—————————\n")
  :init
  (progn
    (global-set-key (kbd "M-x") 'counsel-M-x)
    (global-set-key (kbd "C-c w") 'find-file-other-window)
    (global-set-key (kbd "C-x b") 'counsel-switch-buffer)
    (global-set-key (kbd "C-x b") 'counsel-switch-buffer-other-window)
    (global-set-key (kbd "C-x d") 'counsel-dired)
    (global-set-key (kbd "C-c o") 'dired-other-window)
    (global-set-key (kbd "C-x C-r") 'counsel-recentf)
    (define-key ivy-minibuffer-map (kbd "C-c m") 'ivy-restrict-to-matches)))

(use-package amx
  :config
  (setq amx-save-file "~/.emacs.d/amx-items")
  (setq amx-max-display-length 20)
  (amx-mode 1))

;; remembering history (fast navigation)
(recentf-mode 1)

(use-package swiper
  :after ivy
  :config
  (setq swiper-action-recenter t)
  (setq swiper-goto-start-of-match t)
  (setq swiper-include-line-number-in-search t)
  :bind (("C-S-s" . swiper)))

(use-package helpful
  :commands (helpful-callable helpful-variable helpful-command helpful-key)
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  ([remap describe-function] . counsel-describe-function)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . counsel-describe-variable)
  ([remap describe-key] . helpful-key))

(use-package hydra
  :defer t)

(defhydra hydra-text-scale (:timeout 7)
  "scale text"
  ("j" text-scale-increase "in")
  ("k" text-scale-decrease "out")
  ("f" nil "finished" :exit t))

(etbcf/leader-keys
  "ts" '(hydra-text-scale/body :which-key "scale text"))

(defun etbcf/org-mode-setup ()
  (org-indent-mode)
  (variable-pitch-mode 1)
  (visual-line-mode 1))

(defun etbcf/org-font-setup ()
  ;; replace list hyphen with dot
  (font-lock-add-keywords 'org-mode
                          '(("^ *\\([-]\\) "
                             (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))

  ;; set faces for heading levels
  (dolist (face '((org-level-1 . 1.2)
                  (org-level-2 . 1.1)
                  (org-level-3 . 1.05)
                  (org-level-4 . 1.0)
                  (org-level-5 . 1.1)
                  (org-level-6 . 1.1)
                  (org-level-7 . 1.1)
                  (org-level-8 . 1.1)))
    (set-face-attribute (car face) nil :font "cantarell" :weight 'regular :height (cdr face)))

  ;; ensure that anything that should be fixed-pitch in org files appears that way
  (set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-code nil   :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-table nil   :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch))

(use-package org
  :commands (org-capture org-agenda)
  :hook (org-mode . etbcf/org-mode-setup)
  :config
  (setq org-ellipsis " ▾")
  (etbcf/org-font-setup))

(setq org-agenda-start-with-log-mode t)
(setq org-log-done 'time)
(setq org-log-into-drawer t)

(setq org-agenda-files
      '("~/Dropbox/OrgFiles/Tasks.org"
        "~/Dropbox/OrgFiles/Habits.org"
        "~/Dropbox/OrgFiles/Birthdays.org"
        "~/Dropbox/OrgFiles/Journal.org"))

(require 'org-habit)
(add-to-list 'org-modules 'org-habit)
(setq org-habit-graph-column 60)

(setq org-todo-keywords
      '((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d!)")
        (sequence "BACKLOG(b)" "ACTIVE(a)" "READY(r)" "|" "HOLD(h)" "CANCEL(k@)")))

(setq org-refile-targets
    '(("Archive.org" :maxlevel . 1)
      ("Tasks.org" :maxlevel . 1)))

  ;; save org buffers after refiling!
  (advice-add 'org-refile :after 'org-save-all-org-buffers)

(setq org-tag-alist
    '((:startgroup)
       ; put mutually exclusive tags here
       (:endgroup)
       ("@errand" . ?e)
       ("@home" . ?h)
       ("@work" . ?w)
       ("agenda" . ?a)
       ("planning" . ?p)
       ("publish" . ?p)
       ("batch" . ?b)
       ("note" . ?n)
       ("idea" . ?i)))

;; configure custom agenda views
(setq org-agenda-custom-commands
      '(("d" "Dashboard"
         ((agenda "" ((org-deadline-warning-days 7)))
          (todo "NEXT"
                ((org-agenda-overriding-header "Next Tasks")))
          (tags-todo "agenda/active" ((org-agenda-overriding-header "Active Projects")))))

        ("n" "Next Tasks"
         ((todo "NEXT"
                ((org-agenda-overriding-header "Next Tasks")))))

        ("W" "Work Tasks" tags-todo "+work-email")

        ;; low-effort next actions
        ("e" tags-todo "+TODO=\"NEXT\"+Effort<15&+Effort>0"
         ((org-agenda-overriding-header "Low Effort Tasks")
          (org-agenda-max-todos 20)
          (org-agenda-files org-agenda-files)))

        ("w" "Workflow Status"
         ((todo "BACKLOG"
                ((org-agenda-overriding-header "Project Backlog")
                 (org-agenda-todo-list-sublevels nil)
                 (org-agenda-files org-agenda-files)))
          (todo "READY"
                ((org-agenda-overriding-header "Ready for Work")
                 (org-agenda-files org-agenda-files)))
          (todo "ACTIVE"
                ((org-agenda-overriding-header "Active Projects")
                 (org-agenda-files org-agenda-files)))
          (todo "REVIEW"
                ((org-agenda-overriding-header "In Review")
                 (org-agenda-files org-agenda-files)))
          (todo "CANCEL"
                ((org-agenda-overriding-header "Cancelled Projects")
                 (org-agenda-files org-agenda-files)))))))

(setq org-capture-templates
      `(("t" "Tasks / Projects")
        ("tt" "Task" entry (file+olp "~/Dropbox/OrgFiles/Tasks.org" "Inbox")
         "* todo %?\n  %u\n  %a\n  %i" :empty-lines 1)

        ("j" "Journal Entries")
        ("jj" "Journal" entry
         (file+olp+datetree "~/Dropbox/OrgFiles/Journal.org")
         "\n* %<%i:%m %p> - Journal :journal:\n\n%?\n\n"
         ;; ,(dw/read-file-as-string "~/notes/templates/daily.org")
         :clock-in :clock-resume
         :empty-lines 1)
        ("jm" "Meeting" entry
         (file+olp+datetree "~/Dropbox/OrgFiles/Journal.org")
         "* %<%i:%m %p> - %a :meetings:\n\n%?\n\n"
         :clock-in :clock-resume
         :empty-lines 1)

        ("w" "Workflows")
        ("we" "Checking Email" entry (file+olp+datetree "~/Dropbox/OrgFiles/Journal.org")
         "* Checking Email :email:\n\n%?" :clock-in :clock-resume :empty-lines 1)

        ("m" "Metrics Capture")
        ("mw" "Weight" table-line (file+headline "~/Dropbox/OrgFiles/Metrics.org" "Weight")
         "| %u | %^{Weight} | %^{Notes} |" :kill-buffer t)))

(define-key global-map (kbd "C-c j")
    (lambda () (interactive) (org-capture nil "jj")))

(use-package org-bullets
  :hook (org-mode . org-bullets-mode)
  :custom
  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

(defun etbcf/org-mode-visual-fill ()
  (setq visual-fill-column-width 150
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

(use-package visual-fill-column
  :hook (org-mode . etbcf/org-mode-visual-fill))

(use-package org-wild-notifier
  :custom
  (alert-default-style 'notifications)
  (org-wild-notifier-alert-time '(1 10 30))
  (org-wild-notifier-keyword-whitelist '("TODO"))
  (org-wild-notifier-notification-title "Org wild reminder!")
  :config
  (org-wild-notifier-mode 1))

(with-eval-after-load 'org
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . t)
     (python . t))))

;; automatically tangle emacs.org config file when saved
(defun etbcf/org-babel-tangle-config ()
  (when (string-equal (buffer-file-name)
                      (expand-file-name "~/.emacs.d/Emacs.org"))
    ;; dynamic scope to the rescue
    (let ((org-confirm-babel-evaluate nil))
      (org-babel-tangle))))

      (add-hook 'org-mode-hook (lambda () (add-hook 'after-save-hook #'etbcf/org-babel-tangle-config)))

(with-eval-after-load 'org

  (require 'org-tempo)

  (add-to-list 'org-structure-template-alist '("sh" . "src shell"))
  (add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
  (add-to-list 'org-structure-template-alist '("py" . "src python")))

(use-package auto-package-update
  :custom
  (auto-package-update-interval 7)
  (auto-package-update-prompt-before-update t)
  (auto-package-update-hide-results t)
  :config
  (auto-package-update-maybe)
  (auto-package-update-at-time "21:00"))

;; the default is 800 kilobytes.  measured in bytes.
(setq gc-cons-threshold (* 50 1000 1000))

(defun etbcf/display-startup-time ()
  (message "emacs loaded in %s with %d garbage collections."
           (format "%.2f seconds"
                   (float-time
                     (time-subtract after-init-time before-init-time)))
           gcs-done))

(add-hook 'emacs-startup-hook #'etbcf/display-startup-time)

(use-package lsp-mode
  :hook (web-mode . lsp-deferred)
  :commands (lsp lsp-deferred)
  :init
  (setq lsp-keymap-prefix "C-c l")  ;; or 'C-l', 's-l'
  :config
  (lsp-enable-which-key-integration t))

(use-package lsp-ui
  :hook (lsp-mode . lsp-ui-mode)
  :custom
  (lsp-ui-doc-position 'bottom))

(use-package lsp-treemacs
  :after lsp)

(use-package lsp-ivy
  :after lsp)

(use-package typescript-mode
  :mode "\\.ts\\'"
  :hook (typescript-mode . lsp-deferred)
  :config
  (setq typescript-indent-level 2)
  (require 'dap-node)
  (dap-node-setup)) ;; Automatically installs Node debug adapter if needed

(use-package dap-mode
  ;; Uncomment the config below if you want all UI panes to be hidden by default!
  ;; :custom
  ;; (lsp-enable-dap-auto-configure nil)
  ;; :config
  ;; (dap-ui-mode 1)

  :config
  ;; Set up Node debugging
  (require 'dap-node)
  (dap-node-setup) ;; Automatically installs Node debug adapter if needed

  ;; Bind `C-c l d` to `dap-hydra` for easy access
  (general-define-key
    :keymaps 'lsp-mode-map
    :prefix lsp-keymap-prefix
    "d" '(dap-hydra t :wk "debugger")))

(use-package lsp-pyright
  :hook (python-mode . (lambda ()
			 (require 'lsp-pyright)
			 (lsp-deferred))))  ; or lsp-deferred

;; Define a function to format Python buffer using Black
(defun my/python-format-buffer ()
  "Format Python buffer using Black."
  (interactive)
  (when (eq major-mode 'python-mode)
    (shell-command-on-region (point-min) (point-max) "black -" nil t)))

;; Bind the function to a key
(global-set-key (kbd "<f6>") 'my/python-format-buffer)

(use-package lsp-mode
  :commands lsp
  :hook
  (sh-mode . lsp))

(use-package prettier-js)

(setq web-mode-markup-indent-offset 2)
(setq web-mode-code-indent-offset 2)
(setq web-mode-css-indent-offset 2)

(use-package web-mode
  :mode (("\\.js\\'" . web-mode)
         ("\\.html\\'" . web-mode)
         ("\\.css\\'" . web-mode))
  :commands web-mode
  :config
  (add-hook 'web-mode-hook 'my-web-mode-hook)

  ;; Set up web-mode to recognize embedded CSS
  (setq web-mode-enable-css-colorization t)
  (setq web-mode-enable-css-indentation t))

(use-package emmet-mode)

(defun my-web-mode-hook ()
  "Hook for configuring web-mode."
  (when (or (string-suffix-p ".js" (buffer-file-name))
            (string-suffix-p ".html" (buffer-file-name))
            (string-suffix-p ".css" (buffer-file-name)))
    (prettier-js-mode)
    (emmet-mode)))

(eval-after-load 'web-mode
  '(progn
     (define-key web-mode-map (kbd "C-j") 'emmet-expand-line)))

(add-hook 'web-mode-before-auto-complete-hooks
          '(lambda ()
             (let ((web-mode-cur-language
                    (web-mode-language-at-pos)))
               (if (string= web-mode-cur-language "html")
                   (setq emmet-use-css-transform t)
                 (setq emmet-use-css-transform nil)))))

(setq emmet-indentation 2)

(add-to-list 'load-path "~/.emacs.d/elisp/emacs-browser-refresh")

(require 'browser-refresh)

;; Customize browser-refresh settings
(setq browser-refresh-activate nil)
(setq browser-refresh-default-browser 'firefox)

;; Automatically refresh browser after saving buffer
(defun my-browser-refresh-after-save ()
  (when (and buffer-file-name
             (file-exists-p buffer-file-name)
             (string= major-mode "web-mode"))
    (browser-refresh)))
(add-hook 'after-save-hook #'my-browser-refresh-after-save)

(use-package evil-nerd-commenter
  :bind ("M-/" . evilnc-comment-or-uncomment-lines))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package projectile
  :diminish projectile-mode
  :config (projectile-mode)
  :custom ((projectile-completion-system 'ivy))
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :init
  ;; NOTE: Set this to the folder where you keep your Git repos!
  (when (file-directory-p "~/Projects/Code")
    (setq projectile-project-search-path '("~/Projects/Code")))
  (setq projectile-switch-project-action #'projectile-dired))

(use-package counsel-projectile
  :after projectile
  :config (counsel-projectile-mode))

(use-package magit
  :commands magit-status
  :custom
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

(use-package forge
  :after magit)

(use-package sqlite3)

(setq auth-sources '("~/.authinfo.gpg"))

;; (use-package company
;;   :after lsp-mode
;;   :hook (lsp-mode . company-mode)
;;   :bind (:map company-active-map
;;               ("<tab>" . company-complete-selection))
;;   (:map lsp-mode-map
;;         ("<tab>" . company-indent-or-complete-common))
;;   :custom
;;   (company-minimum-prefix-length 1)
;;   (company-idle-delay 0.0))

(use-package company
    :after lsp-mode
    :hook (lsp-mode . company-mode)
    :bind (:map company-active-map
           ("<tab>" . company-complete-selection))
    :bind (:map lsp-mode-map
           ("<tab>" . company-indent-or-complete-common))
    :custom
    (company-minimum-prefix-length 1)
    (company-idle-delay 0.0)
    :config
    (add-to-list 'company-backends 'company-css)) ; Add company-css to company-backends

(global-set-key (kbd "C-c c") 'company-css) ; keybinding for CSS completion

(use-package company-web
  :after company
  :config
  (add-to-list 'company-backends 'company-web-html)
  (add-to-list 'company-backends 'company-web-css))

(use-package company-box
  :hook (company-mode . company-box-mode))

(use-package company-prescient
  :after company
  :config
  (company-prescient-mode 1))

(use-package term
  :commands term
  :config
  (setq explicit-shell-file-name "bash") ;; Change this to zsh, etc
  ;;(setq explicit-zsh-args '())         ;; Use 'explicit-<shell>-args for shell-specific args

  ;; Match the default Bash shell prompt.  Update this if you have a custom prompt
  (setq term-prompt-regexp "^[^#$%>\n]*[#$%>] *"))

(use-package eterm-256color
  :hook (term-mode . eterm-256color-mode))

(use-package vterm
  :commands vterm
  :config
  (setq terM-prompt-regexp "^[^#$%>\n]*[#$%>] *")  ;; Set this to match your custom shell prompt
  ;;(setq vterM-shell "zsh")                       ;; Set this to customize the shell to launch
  (setq vterM-max-scrollback 10000))

(defun etbcf/configure-eshell ()
  ;; Save command history when commands are entered
  (add-hook 'eshell-pre-command-hook 'eshell-save-some-history)

  ;; Truncate buffer for performance
  (add-to-list 'eshell-output-filter-functions 'eshell-truncate-buffer)

  ;; Bind some useful keys for evil-mode
  (evil-define-key '(normal insert visual) eshell-mode-map (kbd "C-r") 'counsel-esh-history)
  (evil-define-key '(normal insert visual) eshell-mode-map (kbd "<home>") 'eshell-bol)
  (evil-normalize-keymaps)

  (setq eshell-history-size         10000
        eshell-buffer-maximuM-lines 10000
        eshell-hist-ignoredups t
        eshell-scroll-to-bottom-on-input t))

(use-package eshell-git-prompt
  :after eshell)

(use-package eshell
  :hook (eshell-first-time-mode . etbcf/configure-eshell)
  :config

  (with-eval-after-load 'esh-opt
    (setq eshell-destroy-buffer-when-process-dies t)
    (setq eshell-visual-commands '("htop" "zsh" "vim")))

  (eshell-git-prompt-use-theme 'powerline))

(use-package dired
  :ensure nil
  :commands (dired dired-jump)
  :bind (("C-x C-j" . dired-jump))
  :config
  (setq dired-listing-switches "-agho --group-directories-first")
  (evil-collection-define-key 'normal 'dired-mode-map
    "h" 'dired-single-up-directory
    "l" 'dired-single-buffer))

(use-package dired-single
  :commands (dired dired-jump))

(use-package all-the-icons-dired
  :if  (display-graphic-p)
  :hook (dired-mode . all-the-icons-dired-mode))

(setq delete-by-moving-to-trash t)

(use-package dired-open
  :commands (dired dired-jump)
  :config
  ;; Doesn't work as expected!
  ;;(add-to-list 'dired-open-functions #'dired-open-xdg t)
  (setq dired-open-extensions '(("mp4" . "vlc"))))

(use-package dired-hide-dotfiles
  :hook (dired-mode . dired-hide-dotfiles-mode)
  :config
  (evil-collection-define-key 'normal 'dired-mode-map
    "H" 'dired-hide-dotfiles-mode))

;; Make gc pauses faster by decreasing the threshold
(setq gc-cons-threshold (* 2 1000 1000))

(defun my-enable-visual-line-and-flyspell ()
     "Enable visual-line-mode and flyspell-mode if buffer name ends with '.txt'."
     (when (and (buffer-file-name)
                 (string-suffix-p ".txt" (buffer-file-name)))
        (visual-line-mode 1)
        (flyspell-mode 1)))

   (add-hook 'text-mode-hook 'my-enable-visual-line-and-flyspell)
